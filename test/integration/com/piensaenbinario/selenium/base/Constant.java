package com.piensaenbinario.selenium.base;

import org.openqa.selenium.By;

public class Constant {

	/**
	 * Elemento form del seleniumtest
	 */
	public static final By DOM_FORM_FM = By.name("_seleniumtest_WAR_seleniumtestportlet_fm");
	
	/**
	 * Elemento input method del formulario del seleniumtest
	 */
	public static final By DOM_FORM_METHOD = By.name("_seleniumtest_WAR_seleniumtestportlet_method");
	
	/**
	 * Elemento input params del formulario del seleniumtest
	 */
	public static final By DOM_FORM_PARAMS = By.name("_seleniumtest_WAR_seleniumtestportlet_params");
	
	/**
	 * Elemento input service del formulario del seleniumtest
	 */
	public static final By DOM_FORM_SERVICE = By.name("_seleniumtest_WAR_seleniumtestportlet_service");
	
	/**
	 * Elemento input login del formulario de login
	 */
	public static final By DOM_LOGIN_NAME = By.id("_58_login");
	
	/**
	 * Elemento input password del formulario de login
	 */
	public static final By DOM_LOGIN_PASS = By.id("_58_password");
	
	/**
	 * Elemento form de login
	 */
	public static final By DOM_LOGIN_FORM = By.id("_58_fm");
	
	/**
	 * Elemento mensaje de error de un porlet
	 */
	public static final By DOM_ALERT_ERROR = By.className("alert-error");
	
	/**
	 * Elemento mensaje de exito de un porlet
	 */
	public static final By DOM_ALERT_SUCCESS = By.className("alert-success");
	
}