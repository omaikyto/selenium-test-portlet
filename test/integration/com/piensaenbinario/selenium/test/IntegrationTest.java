package com.piensaenbinario.selenium.test;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.piensaenbinario.selenium.base.Constant;

public class IntegrationTest {

	private WebDriver driver;
	Properties prop = new Properties();
	
	@Before
    public void setUp() throws Exception {
    
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		capabilities.setCapability("platform", Platform.WINDOWS);
        capabilities.setCapability("version", "46");
        capabilities.setCapability("browserName", "firefox");
	
        InputStream input = IntegrationTest.class.getClassLoader().getResourceAsStream("config.properties");
        prop.load(input);
        
        driver = new FirefoxDriver(capabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test 
    public void doLogin() throws Exception {

    	driver.get(prop.getProperty("domain")+"/c/portal/login");

        WebElement name = driver.findElement(Constant.DOM_LOGIN_NAME);
        name.clear();
        name.sendKeys(prop.getProperty("user"));
        
        WebElement password = driver.findElement(Constant.DOM_LOGIN_PASS);
        password.clear();
        password.sendKeys(prop.getProperty("pass"));
        
        WebElement form = driver.findElement(Constant.DOM_LOGIN_FORM);
        form.submit();
        
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> msgsError = driver.findElements(Constant.DOM_ALERT_ERROR);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		assertEquals(0, msgsError.size());
    }
    
    @Test
    public void fecthUser() throws Exception {
    	
    	driver.get(prop.getProperty("domain"));
    	
    	WebElement service = driver.findElement(Constant.DOM_FORM_SERVICE);
    	service.clear();
    	service.sendKeys("com.liferay.portal.service.UserLocalServiceUtil");
        
        WebElement method = driver.findElement(Constant.DOM_FORM_METHOD);
        method.clear();
        method.sendKeys("fetchUser");
        
        WebElement params = driver.findElement(Constant.DOM_FORM_PARAMS);
        params.clear();
        params.sendKeys("20801");
        
        WebElement form = driver.findElement(Constant.DOM_FORM_FM);
        form.submit();
        
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> msgsSuccess = driver.findElements(Constant.DOM_ALERT_SUCCESS);
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		assertEquals(1, msgsSuccess.size());
    }
    
    @After
    public void tearDown() throws Exception {
       
    	driver.quit();
    }
   	
}