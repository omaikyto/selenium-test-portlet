package com.piensaenbinario.selenium;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class SeleniumTestPortlet
 */
public class SeleniumTestPortlet extends MVCPortlet {
 
	public void serviceTest(ActionRequest actionRequest, ActionResponse actionResponse)	
			throws InvocationTargetException, ClassNotFoundException, InstantiationException, 
			IllegalAccessException, NoSuchMethodException, SecurityException{
		
		String method = ParamUtil.getString(actionRequest, "method");
		String[] params = ParamUtil.getParameterValues(actionRequest, "params");
		String service = ParamUtil.getString(actionRequest, "service");
		Class<?> serviceUtilClass = Class.forName(service);
		Object serviceUtilInstance = serviceUtilClass.newInstance();
		Method[] methods = serviceUtilClass.getMethods();
		List<Object> parameters = new ArrayList<Object>();
		
		for (Method m : methods) {
			if( m.getName().equals(method) && Validator.equals(m.getParameterTypes().length, params.length) ){
				Class<?>[] types = m.getParameterTypes();
				for (int i = 0; i < types.length; i++) {
					if ( types[i].equals(Long.class) || types[i].equals(long.class) ){
						parameters.add(GetterUtil.getLong(params[i]));
					}else if( types[i].equals(String.class) ){
						parameters.add(GetterUtil.getString(params[i]));
					}else if( types[i].equals(Double.class) || types[i].equals(double.class) ){
						parameters.add(GetterUtil.getDouble(params[i]));
					}else if( types[i].equals(Integer.class) || types[i].equals(int.class) ){
						parameters.add(GetterUtil.getInteger(params[i]));
					}else if( types[i].equals(Boolean.class) || types[i].equals(boolean.class) ){
						parameters.add(GetterUtil.getBoolean(params[i]));
					}
				}
				setMethod(m);
				break;
			}
		}
		
		switch (params.length) {
			case 0:
				setResult(getMethod().invoke(serviceUtilInstance));
				break;
			case 1:
				setResult(getMethod().invoke(serviceUtilInstance, parameters.get(0)));
				break;
			default:
				break;
		}
		
		actionResponse.setRenderParameter("result", getResult().toString());
		actionResponse.setRenderParameter("type", getMethod().getReturnType().getName());
	}

	private static Object result = null;
	private static Method method = null;
	
	public static Object getResult() {
		return result;
	}
	
	public static void setResult(Object result) {
		SeleniumTestPortlet.result = result;
	}
	
	public static Method getMethod() {
		return method;
	}
	
	public static void setMethod(Method method) {
		SeleniumTestPortlet.method = method;
	}
	
}