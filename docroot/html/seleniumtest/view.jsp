<%@ include file="init.jsp" %>

<%
	String result = ParamUtil.getString(request, "result");
	String type = ParamUtil.getString(request, "type");
%>
	<div class="row-fluid">
		<div class="span6">
			<liferay-portlet:actionURL name="serviceTest" var="serviceTestURL" />
			<aui:form name="fm" action="<%=serviceTestURL %>" method="post" >
				<aui:input name="service" label="Nombre de la clase Util" />
				<aui:input name="method" label="Nombre del m�todo" />
				<aui:input name="params" label="Lista de par�metros" />
				<aui:button type="submit" value="Probar" />
			</aui:form>
		</div>
		<div class="span6">
			<span class="field-label">
				<liferay-ui:message key="Resultado" arguments="<%=new Object[] {type} %>"/>
			</span>
			<span id="result" class="field-input field-input-text" style="display:block">
				<%=result %>
			</span>
		</div>
	</div>